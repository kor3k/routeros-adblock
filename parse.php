<?php

$lines  =   file( 'http://winhelp2002.mvps.org/hosts.txt' );
$custom =   explode(',', $_REQUEST['custom']);

echo '<pre>';

if( 'dns' == $_REQUEST['type'] )
{
    if( empty( $_REQUEST['ip'] ) )
        throw new \Exception( 'you must define an ip' );

    echo "/ip firewall filter add action=reject chain=forward comment=adblock dst-address={$_REQUEST['ip']} reject-with=icmp-network-unreachable";
    echo PHP_EOL;

    if(!empty($_REQUEST['clear'])) {
        echo '/ip dns static remove [find comment="adblock"]';
        echo PHP_EOL;
    }

    echo '/ip dns static';
    $cmd    =   "add address={$_REQUEST['ip']} comment=adblock name=";
}
else if( 'addrlist' == $_REQUEST['type'] )
{
    if( empty( $_REQUEST['list'] ) )
        throw new \Exception( 'you must define an address list name' );

    echo "/ip firewall filter add action=reject chain=forward comment=adblock dst-address-list={$_REQUEST['list']} reject-with=icmp-network-unreachable";
    echo PHP_EOL;

    if(!empty($_REQUEST['clear'])) {
        echo '/ip firewall address-list remove [find list="'.$_REQUEST['list'].'"]';
        echo PHP_EOL;
    }

    echo '/ip firewall address-list';
    $cmd    =   "add list={$_REQUEST['list']} address=";
}
else throw new \Exception( 'wrong "type" parameter (dns|addrlist)' );

echo PHP_EOL;

foreach($custom as $key => $line) {
    echo $cmd.$line;
    echo PHP_EOL;
}

foreach( $lines as $key => $line )
{
    if( 0 !== strpos( $line , '0' ) )
        continue;

    $host   =   trim( substr( $line , 8 ) );

    if( false !== $p = strrpos( $host , '#' ) )
        $host   =   substr( $host , 0 , $p );

    echo $cmd.$host;
    echo PHP_EOL;
}

echo '</pre>';

